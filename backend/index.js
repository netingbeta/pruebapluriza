const express = require("express");
const PORT = process.env.PORT || 5000
const app = express();

	//NOTA: realice la conexión a redis, por que ya no disponen bases de datos mongodb gratis, minimo agregar tarjeta de credito y no tengo
	//Si me proprociona una conexión mongodb le demuestro si manejo el Nosql
	
var request = require("request");
	redis = require('redis'),
	redisClient = redis.createClient({host : 'redis-13675.c1.us-west-2-2.ec2.cloud.redislabs.com', port : 13675});
	
redisClient.auth('vHECcllreVwLbPHsX6Smtr0wYLU8LWik',function(err,reply) {
	if(!err) {
		console.log(Date(),"Conectado correctamente a la base de datos");
	}else{
		console.log(Date(),'Error en la conexión de base de datos ',err);
	}
});

redisClient.on('ready',function() {
	console.log(Date(),"Activa la base de datos");
});

redisClient.on('error',function() {
	console.log(Date(),"Descontada la base de datos");
});

app.use(function (req, res, next) {
	console.log(Date(),"Configuro el uso de headers y mas");
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    next();
});

console.log(Date(),"Acceso a GetToken Ok");
app.get('/getToken', function (req, res) {

	var ejecucion = require('./modelos/getToken');
	ejecucion(request).then(function(info) {
		res.send(info);
	}).catch(function(err) {
		res.send(err);
	});
			
});

console.log(Date(),"Acceso a GetAuth Ok");
app.get('/getAuth', function (req, res) {
	
	var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress,
		ip = ip.replace(".","_"),
		ip = ip.replace(".","_"),
		ip = ip.replace(".","_"),
		registro = ip.replace(".","_");
	
	redisClient.get("usuario_temporal_"+registro,function(error2,datos){
		
		if(datos!==null){
			
			var info = JSON.parse(datos);
			info[1]="";
			res.send(info);
			
		}else{
			
			if(req.query.oauth_token!==undefined && req.query.oauth_verifier!==undefined){
				var ejecucion = require('./modelos/getAuth');
				ejecucion(req.query.oauth_token,req.query.oauth_verifier,request).then(function(info) {
					
					redisClient.set("usuario_temporal_"+registro,JSON.stringify(info),function(errores,replica){
						redisClient.expire("usuario_temporal_"+registro, 60 * 60);
						info[1]="";
						res.send(info);
					});	
					
				}).catch(function(err) {
					res.send(err);
				});
			}
			
		}
		
	});
	
});

console.log(Date(),"Acceso a get100Tweets Ok");
app.get('/get100Tweets', function (req, res) {
	
	var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress,
		ip = ip.replace(".","_"),
		ip = ip.replace(".","_"),
		ip = ip.replace(".","_"),
		registro = ip.replace(".","_");
	
	redisClient.get("usuario_temporal_"+registro,function(errores,ejecucion){
		if(ejecucion!==null){
			
			var extra = JSON.parse(ejecucion),
				todo = extra[1].split("&"),
				oauth_token = todo[0].split("="),
				oauth_token_secret = todo[1].split("="),
				screen_name = todo[3].split("=");
				
			var ejecucion = require('./modelos/get100Tweets');
			ejecucion(oauth_token[1],oauth_token_secret[1],screen_name[1],request).then(function(info) {
				res.send(info);
			}).catch(function(err) {
				res.send(err);
			});
		}
	});	
	
});


app.listen(PORT, () => {
	console.log(Date(),"El servidor está inicializado en el puerto 5000");
});