eje = function(oauth_tokenInput,oauth_verifierInput,request) {
	return new Promise(function(resolve, reject) {
		
		console.log(Date(),"Verificación de token access");

		var options = { method: 'POST',
		  url: 'https://api.twitter.com/oauth/access_token',
		  headers: 
		   { 'cache-control': 'no-cache',
			 'content-type': 'application/x-www-form-urlencoded' },
		  form: 
		   { oauth_consumer_key: 'boURlpNnrrsA5NPsdGTpxxuLI',
			 oauth_token: oauth_tokenInput,
			 oauth_verifier: oauth_verifierInput } };
		

		request(options, function (error, response, body) {
			if (error){
			  reject([false,error]);
			}else{
				if(body.indexOf("temporalmente")==-1 && body.indexOf("Request token missing")==-1 && body.indexOf("unavailable")==-1){
					
					console.log(Date(),"Exito la verificación de access_token ", body);
					
					var timestamp = Math.floor(Date.now() / 1000),
						nonce = 'abcdef'.split('').map(function(v,i,a){return i>5 ? null : a[Math.floor(Math.random()*5)] }).join('');
							
					var dividir = body.split("&"),
						oauth_tokenB = dividir[0].split("="),
						oauth_token_secret = dividir[1].split("="),
						nameis = dividir[3].split("=");
						
					var parameters = {
						oauth_consumer_key : 'boURlpNnrrsA5NPsdGTpxxuLI',
						oauth_token : oauth_tokenB[1],
						oauth_signature_method : 'HMAC-SHA1',
						oauth_timestamp: timestamp,
						oauth_version : '1.0',
						oauth_nonce: nonce,
						name: nameis[1]
					};
					
					var oauthSignature = require("oauth-signature");
					var sig = oauthSignature.generate("POST","https://api.twitter.com/1.1/account/update_profile.json", parameters, "h9pwiJubqfu23IzF3SHzYSeCrs6kuXjSEumQ6vSOoixBOyln00", oauth_token_secret[1],{ encodeSignature: true});
					console.log(Date(),"Creacíon de signature ",sig);

					var options = { method: 'POST',
					  url: 'https://api.twitter.com/1.1/account/update_profile.json',
					  qs: { name: nameis[1] },
					  headers: 
					   { 'cache-control': 'no-cache',
						 authorization: 'OAuth oauth_consumer_key="'+parameters.oauth_consumer_key+'",oauth_token="'+oauth_tokenB[1]+'",oauth_signature_method="HMAC-SHA1",oauth_timestamp="'+timestamp+'",oauth_nonce="'+nonce+'",oauth_version="1.0",oauth_signature="'+sig+'"' } };

					request(options, function (error2, response2, body2) {
						if (error2){
							console.log(Date(),"Error en extración de datos personales ",error2);
							reject([false,error2]);
						}else{
							console.log(Date(),"Exito ha extraido los datos del perfil ",body2);
							resolve([true,body,body2]);
						}
					});
					
					
				}else{
					console.log(Date(),"El token se ha vencido o ha sido usado mas de una ves");
					reject([false,"El token se ha vencido o ha sido usado mas de una ves"]);
				}
			}
		});
		
	});
};

module.exports = eje;