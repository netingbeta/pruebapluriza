eje = function(oauth_tokenInput,oauth_token_secretInput,screen_nameInput,request) {
	return new Promise(function(resolve, reject) {
				
		var timestamp = Math.floor(Date.now() / 1000);
		var nonce = 'abcdef'.split('').map(function(v,i,a){return i>5 ? null : a[Math.floor(Math.random()*5)] }).join('');
				
		var parameters = {
			oauth_consumer_key : 'boURlpNnrrsA5NPsdGTpxxuLI',
			oauth_token : oauth_tokenInput,
			oauth_signature_method : 'HMAC-SHA1',
			oauth_timestamp: timestamp,
			oauth_version : '1.0',
			oauth_nonce: nonce,
			q: screen_nameInput
		};
		
		var oauthSignature = require("oauth-signature");
		var sig = oauthSignature.generate("GET","https://api.twitter.com/1.1/search/tweets.json", parameters, "h9pwiJubqfu23IzF3SHzYSeCrs6kuXjSEumQ6vSOoixBOyln00", oauth_token_secretInput,{ encodeSignature: true});
		
		console.log(Date(),"Creacíon de signature ",sig);

		var options = { method: 'GET',
		  url: 'https://api.twitter.com/1.1/search/tweets.json',
		  qs: { q: screen_nameInput },
		  headers: 
		   { 'cache-control': 'no-cache',
			 authorization: 'OAuth oauth_consumer_key="boURlpNnrrsA5NPsdGTpxxuLI",oauth_token="'+oauth_tokenInput+'",oauth_signature_method="HMAC-SHA1",oauth_timestamp="'+timestamp+'",oauth_nonce="'+nonce+'",oauth_version="1.0",oauth_signature="'+sig+'"' } };
				
		request(options, function (error2, response2, body2) {
			if (error2){
				console.log(Date(),"Error en extración de 100 tweets ",error2);
				reject([false,error2]);
			}else{
				console.log(Date(),"Exito ha extraido los datos del perfil ",body2);
				resolve([true,body2]);
			}
		});
		
	});
};

module.exports = eje;